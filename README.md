# Background

Just playing around getting familiar with mongodb in docker and how it works with the reactive framework

# Running it
Start mongodb with `docker-compose up`
Start project in your IDE of choice (I'm using intellij) or run jar file:
`mvn clean package`
`java -jar reactivebackend-0.0.1-SNAPSHOT.jar`

## Create fake data
Depending on your setup you can create fake data suiting your volume needs.
docker has a volume setup and data will be saved between restarts.
To create 100 entries:
`curl -X POST http://localhost:8080/random/100`
If you overdo it, you can deleteall data with:
`curl -X DELETE http://localhost:8080/random`

## Testing reactive stuff
This is tested in chrome and firefox. To avoid having to put to much data into this test I have made an artificial 
delay between elements in se.stralfors.reactivebackend.controllers.DataEntryController.getAllDataEntries. 
This should allow you to watch elements stream in. 
Visit:
`http://localhost:8080/entries/all`