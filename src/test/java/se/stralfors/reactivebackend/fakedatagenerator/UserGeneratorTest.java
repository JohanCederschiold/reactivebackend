package se.stralfors.reactivebackend.fakedatagenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import reactor.core.publisher.Flux;
import se.stralfors.reactivebackend.dto.DataEntry;

import java.util.ArrayList;
import java.util.List;

class UserGeneratorTest {

    UserGenerator generator;

    @BeforeEach
    void setUp () {
        generator = new UserGenerator();
    }

    @Test
    void generateDataEntries() {
        int volume = 100;
        Flux<DataEntry> reply =  generator.generateDataEntries(volume);
        final List<DataEntry> generated = new ArrayList<>();
        reply
                .doOnNext(generated::add)
                .subscribe();
        Assertions.assertEquals(generated.size(), volume);
    }
}