package se.stralfors.reactivebackend.fakedatagenerator;

import com.github.javafaker.Faker;
import reactor.core.publisher.Flux;
import se.stralfors.reactivebackend.dto.DataEntry;

public class UserGenerator {

    Faker faker = new Faker();

    public Flux<DataEntry> generateDataEntries(int noEntries) {
        return Flux.range(0, noEntries)
                .map(ignored -> generatePerson());
    }

    public DataEntry generatePerson () {
        var entry = new DataEntry();
        entry.setFirstname(faker.name().firstName());
        entry.setLastname(faker.name().lastName());
        entry.setOccupation(faker.job().title());
        entry.setAge(faker.number().numberBetween(18, 100));
        return entry;
    }
}
