package se.stralfors.reactivebackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;
import se.stralfors.reactivebackend.fakedatagenerator.UserGenerator;
import se.stralfors.reactivebackend.repositories.DatabaseRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class FakeDataServiceImplementation implements FakeDataService{

    DatabaseRepository repository;

    @Autowired
    public FakeDataServiceImplementation(DatabaseRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<DataEntry> generateUser() {
        return repository.insert(new UserGenerator().generatePerson());
    }

    @Override
    public Flux<DataEntry> enterMultipleData(int numberOfEntries) {
        List<DataEntry> entries = new ArrayList<>();
        var generator = new UserGenerator();
        for (int i = 0 ; i < numberOfEntries ; i++ ) {
            entries.add(generator.generatePerson());
        }
        return repository.insert(entries);
    }

    @Override
    public void deleteAllEntries() {
        repository.deleteAll().subscribe();
    }
}
