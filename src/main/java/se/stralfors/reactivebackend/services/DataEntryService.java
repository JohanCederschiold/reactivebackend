package se.stralfors.reactivebackend.services;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;

public interface DataEntryService {

    public Mono<DataEntry> enterData(DataEntry dataEntry);

    public void deleteData(String dataId);

    public Mono<DataEntry> getSpecificData(String dataId); 

    public Flux<DataEntry> getDataMatchingParameters(String firstname, String lastname);

    public Flux<DataEntry> getAllEntries();
}
