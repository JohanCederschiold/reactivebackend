package se.stralfors.reactivebackend.services;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;

public interface FakeDataService {

    public Mono<DataEntry> generateUser();

    public Flux<DataEntry> enterMultipleData(int numberOfEntries);

    public void deleteAllEntries();
}
