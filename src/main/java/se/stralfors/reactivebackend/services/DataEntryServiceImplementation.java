package se.stralfors.reactivebackend.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;
import se.stralfors.reactivebackend.repositories.DatabaseRepository;

import java.time.Duration;
import java.util.function.Consumer;

@Service
public class DataEntryServiceImplementation  implements DataEntryService{

    Logger logger = LoggerFactory.getLogger(DataEntryServiceImplementation.class);

    DatabaseRepository repository;

    @Autowired
    public DataEntryServiceImplementation(DatabaseRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<DataEntry> enterData(DataEntry dataEntry) {
        logger.info("Attempting to insert {}", dataEntry);
        return repository.insert(dataEntry);
    }

    @Override
    public void deleteData(String dataId) {
        logger.info("Attempting to delete {}", dataId);
        repository.deleteById(dataId)
                .doOnSuccess(ignore -> infoLogThis.accept("Deleted: " + dataId))
                .subscribe();
    }
    @Override
    public Mono<DataEntry> getSpecificData(String dataId) {
        logger.info("Attempting to get {}", dataId);
        return repository.findById(dataId);
    }

    @Override
    public Flux<DataEntry> getDataMatchingParameters(String firstname, String lastname) {
        logger.info("Attempting to find {} {}", firstname, lastname);
        return repository.findByFirstnameAndLastname(firstname, lastname);
    }

    @Override
    public Flux<DataEntry> getAllEntries() {
        logger.info("Attempting to return all data");
        return repository.findAll().delayElements(Duration.ofMillis(1000));
    }

    protected Consumer<String> infoLogThis= msg -> logger.info(msg);

}
