package se.stralfors.reactivebackend.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import se.stralfors.reactivebackend.dto.DataEntry;

@Repository
public interface DatabaseRepository extends ReactiveMongoRepository<DataEntry, String> {

    Flux<DataEntry> findByFirstnameAndLastname(String firstname, String lastname);
}
