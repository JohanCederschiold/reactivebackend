package se.stralfors.reactivebackend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;
import se.stralfors.reactivebackend.services.DataEntryService;

import java.time.Duration;

@RestController
@RequestMapping("/entries")
public class DataEntryController {

    DataEntryService service;

    @Autowired
    public DataEntryController(DataEntryService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Mono<DataEntry> getSpecificEntry(@PathVariable(value = "id") String id) {
        return service.getSpecificData(id);
    }

    @GetMapping("/all")
    public Flux<DataEntry> getAllDataEntries() {
        return service.getAllEntries()
                .delayElements(Duration.ofMillis(100));
    }

    @GetMapping("/name")
    public Flux<DataEntry> getEntriesMatchingCriteria(@RequestParam(value = "first") String firstname, @RequestParam(value = "last") String lastname) {
        return service.getDataMatchingParameters(firstname, lastname);
    }

    @PostMapping("/create")
    public Mono<DataEntry> makeDataEntry (@RequestBody DataEntry dataEntry) {
        return service.enterData(dataEntry);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteDataEntry(@PathVariable(value = "id") String id) {
        service.deleteData(id);
    }
}
