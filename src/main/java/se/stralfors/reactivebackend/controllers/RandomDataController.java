package se.stralfors.reactivebackend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.stralfors.reactivebackend.dto.DataEntry;
import se.stralfors.reactivebackend.services.FakeDataService;

@RestController
@RequestMapping("random")
public class RandomDataController {

    FakeDataService service;

    @Autowired
    public RandomDataController(FakeDataService service) {
        this.service = service;
    }

    @PostMapping()
    public Mono<DataEntry> insertRandomUser() {
        return service.generateUser();
    }

    @PostMapping("/{volume}")
    public Flux<DataEntry> insertRandomUsers(@PathVariable(value = "volume") int volume) {
        return service.enterMultipleData(volume);
    }

    @DeleteMapping
    public void deleteAll() {
        service.deleteAllEntries();
    }
}
